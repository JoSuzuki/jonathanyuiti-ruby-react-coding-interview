require 'rails_helper'

RSpec.describe "API Tweets", type: :request do

  # See user_request_spec for inspiration
  describe "#search" do
    let!(:user) { create(:user) }
    let!(:matching_tweet) { create(:tweet, user_id: user.id, body: 'Hey friend') }
    let!(:not_matching_tweet) { create(:tweet, user_id: user.id, body: 'Something else') }

    let(:result) { JSON.parse(response.body) }

    context 'when searching for tweets' do

      it 'returns tweets matching the search' do
        get api_tweets_path(search: 'Hello')

        expect(result.size).to eq(1)
        expect(result).to eq([])
      end
    end
  end
end
