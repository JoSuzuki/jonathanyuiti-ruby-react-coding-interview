class Tweet < ApplicationRecord
  belongs_to :user

  validates :user_id, presence: true
  validates :body, presence: true

  scope :keyword_search, ->(search_query) { where("body like ?", "%#{search_query}%") }

end
