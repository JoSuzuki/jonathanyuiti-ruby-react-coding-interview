class HomeController < ApplicationController
  def index
    set_tweets
  end

  def create
    @tweet = Tweet.new(user_id: current_user.id, body: params[:body])

    if @tweet.save
      redirect_back(fallback_location: root_path)
    else
      set_tweets
      render :index, status: :unprocessable_entity
    end
  end

  private

  def set_tweets
    @tweets = Tweet.distinct(:user_id).order("created_at DESC").to_a.first(20)
  end
end
